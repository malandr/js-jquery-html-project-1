
function Cat(id, name, age, color) {
    this.id = id;
    this.name = name;
    this.age = age;
    this.color = color;
}

var liza = new Cat(1, 'Liza', 10, 'Three-colored');
var snizhok = new Cat(2, 'Snizhok', 9, 'White');
var bagira = new Cat(3, 'Bagira', 9, 'Black');

var catArr = [liza, bagira, snizhok];

(function ($) {

    var divToAdd = '<div class="to_be_added">some para</div>';

    var selectValue = $('#select').val();

    // Show cat after clicking Show

    $('#cat_button').on('click', function () {
        $('.cat_div').animate({ height: '100px' });
        $('#cat_para').append('<br>');
        $('#cat_para').append(liza.name + '<br>');
        $('#cat_para').append(liza.age + '<br>');
        $('#cat_para').append(liza.color + '<br>');
    });

    // Show cat after name input and click

    $('#button_add').on('click', function () {

        var catFromInput = $('#cat_name').val();

        if (catFromInput == 'Liza') {
            $('#before').append(liza.name + '<br>');
            $('#before').append(liza.age + '<br>');
            $('#before').append(liza.color + '<br>');
        } else if (catFromInput == 'Bagira') {
            $('#before').append(bagira.name + '<br>');
            $('#before').append(bagira.age + '<br>');
            $('#before').append(bagira.color + '<br>');
        } else if (catFromInput == 'Snizhok') {
            $('#before').append(snizhok.name + '<br>');
            $('#before').append(snizhok.age + '<br>');
            $('#before').append(snizhok.color + '<br>');
        }

    });

    // Themes

    $('.radio_menu label').on('click', function () {
        // $('#before').append($(this).attr('for'));
        switchTheme($(this).attr('for'))
    });

    function switchTheme(value) {
        $('body').toggleClass('dark');
    }

    // function switchTheme(value) {
    //     if (value == 'light') {
    //         $('body').addClass('light');
    //     } else {
    //         $('body').addClass('dark');
    //     }
    // }


    // Show cat by selected Name automatically

    $('#cat_select').on('change', function (event) {

        // var divToAdd = $('.to_be_added');

        // for (let i = 0; i < selectValue; i++) {
        //     $('.to_add_to').append(divToAdd);
        //     // $('.to_add_to').append(divToAdd);
        // }

        for (let i = 0; i < catArr.length; i++) {
            if ($(this).val() == catArr[i].name) {
                $('.cat_div_last').animate({ height: '100px' });
                $('.cat_div_last').append('<br>');
                $('.cat_div_last').append(catArr[i].name + '<br>');
                $('.cat_div_last').append(catArr[i].age + '<br>');
                $('.cat_div_last').append(catArr[i].color + '<br>');
            }
        }

    }).stop();

    
    

})(jQuery);