
function Person (name, age, address) {
    this.name = name;
    this.age = age;
    this.address = address;
}

var vasyl = new Person('vasyl', 45, 'some address');
var ignat = new Person('Ignat', 34, 'address ssss');

var persons = [vasyl, ignat];

for (var g in persons) {
    console.log(persons[g]);    
}

function test (phrase) {
    return phrase;
}

var arr = ['hello', 14, 'phrase'];

let counter = 0;

while (counter < arr.length) {
    console.log(arr[counter]);
    counter ++;
}

var andrii = {
    name : 'Andrii',
    age : 31,
    getAge : function() {return andrii.age}
}

// var getAge = function () {return andrii.age};
console.log(andrii.getAge());

console.log(test('TEST!'));

