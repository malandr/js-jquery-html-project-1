
let main = document.getElementById('main');
let mainPara = document.getElementById('main-para');

let newDiv = document.createElement('div');
let mySelect = document.getElementById('my-select');

newDiv.className = 'new-div-class';

function populateData() {
    newDiv.innerHTML = '<p>New added text</p>';
    document.body.append(newDiv);
    document.body.appendChild(newDiv);
};

main.before('Added before!');

mainPara.addEventListener('click', populateData());


for (let i = 0; i < main.childNodes.length; i++) {
    console.log(main.childNodes[i]);
    
}

// console.log('inner text ===' + main.innerText);
// console.log('inner HTML ===' + main.innerHTML);
// console.log('closest ===' + mainPara.closest('div').id);
// console.log('attribute ===' + main.getAttributeNames());
// console.log('node name ===' + mainPara.nodeName);
// console.log('node type ===' + mainPara.nodeType);
// console.log('node value ===' + mainPara.nodeValue);
// console.log('title ===' + mainPara.tex);

// array1.forEach(element => console.log(element.nodeName));
// console.log(mySelect.options[mySelect.selectedIndex].innerHTML);

// let selectText = mySelect.options[mySelect.selectedIndex].text;
// console.log(selectText);
// let selectValue = mySelect.options[mySelect.selectedIndex].value;
// console.log(selectValue);
