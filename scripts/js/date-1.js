

var Xmas95 = new Date('December 25, 1995 23:15:30');

var weekday = Xmas95.getDay();
var month = Xmas95.getMonth();

// console.log(weekday); 

var optionsDay = { weekday: 'long'};
var optionsMonth = { month: 'long'};

console.log(new Intl.DateTimeFormat('en-US', optionsDay).format(Xmas95));
// console.log(new Intl.DateTimeFormat('de-DE', options).format(Xmas95));

console.log(new Intl.DateTimeFormat('en-US', optionsMonth).format(Xmas95));
// December
// console.log(new Intl.DateTimeFormat('de-DE', optionsMonth).format(Xmas95));
// Dezember
