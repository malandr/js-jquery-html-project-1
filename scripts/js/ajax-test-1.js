
(function ($) {

    // ======= OPTION 1 =======

    $('#button_add_rest').on('click', function(){

        var inputString = $('#rest_input').val();

        console.log(inputString);        

        $.getJSON("https://swapi.co/api/" + inputString, function (data) {

            var jsonKeys = Object.keys(data.results[0]);

            $.each(data.results, function (index, element) {
    
                for (let g in jsonKeys) {
                    $('.to_add_to').append(jsonKeys[g] + ' == ' + element[jsonKeys[g]] + '<br>');
                }
    
                $('.to_add_to').append('---------------------------------' + '<br>');
            });
    
            console.log(data)
    
        });

    });   

    // ======= OPTION 2 =======

    // $.ajax({
    //     url: 'https://swapi.co/api/planets',
    //     type: 'get',
    //     dataType: 'json',
    //     error: function (data) {
    //     },
    //     success: function (data) {

    //         // var jsonKeys = [];

    //         // for(var key in data) {
    //         //     keys.push(key);

    //         // }

    //         var jsonKeys = Object.keys(data.results[0]);

    //         $.each(data.results, function (index, element) {

    //             for (let g in jsonKeys) {
    //                 $('.to_add_to').append(jsonKeys[g] + ' == ' + element[jsonKeys[g]] + '<br>');
    //             }

    //             $('.to_add_to').append('---------------------------------' + '<br>'F);
    //         });

    //     }
    // });

    $.getJSON("https://swapi.co/api/people", function (data) {

        console.log(data.results);

        var luke = data.results[0];
        var darth = data.results[3];
        var obi = data.results[9];

        var $column1 = $('#column_1');
        var $column2 = $('#column_2');
        var $column3 = $('#column_3');

        $column1.append(luke.name);
        $column2.append(data.results[3].name);
        $column3.append(data.results[9].name);

        $column1.one('click', function() {
            $column1.append('<br>');
            $column1.append('Birth Year: ' + luke.birth_year + '<br>');
            $column1.append('Height: ' + luke.height +'<br>');
        })

        $column2.one('click', function() {
            $column2.append('<br>');
            $column2.append('Birth Year: ' + darth.birth_year + '<br>');
            $column2.append('Height: ' + darth.height +'<br>');
        })

        $column3.one('click', function() {
            $column3.append('<br>');
            $column3.append('Birth Year: ' + obi.birth_year + '<br>');
            $column3.append('Height: ' + obi.height +'<br>');
        })

        $column1.on('click', function(){
            // $('#column_1').animate( { height: '100px' } );            
            $column1.toggleClass('my_column_expanded');
    
        });

        $column2.on('click', function(){
            // $('#column_1').animate( { height: '100px' } );            
            $column2.toggleClass('my_column_expanded');
    
        });

        $column3.on('click', function(){
            // $('#column_1').animate( { height: '100px' } );            
            $column3.toggleClass('my_column_expanded');
    
        });

    });

})(jQuery);